<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog from sractch</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php
//pour afficher les erreurs
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

<header>
        <?php
            //pour lier le header :
            include __DIR__.'/../includes/header.php';
        ?>
    </header>
    <main>
    
    <?php
    require ('configuration.php');
    $idPage = $_GET['id'];

    // On se connecte à MySQL
    $bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch',$myUserName,$myPassWord);
    

  
    // On récupère tout le contenu de la table articles
    $reponse = $bdd->query('
    SELECT *
    FROM articles
    WHERE id =' .$idPage);

    $auteur = $bdd->query('
    SELECT *
    FROM authors
    JOIN articles ON articles.author_id=authors.id
    WHERE articles.id =' .$idPage);

    $category = $bdd->query('
    SELECT *
    FROM categories
    JOIN articles_categories ON categories.id=articles_categories.category_id
    WHERE articles_categories.article_id=' .$idPage);

    $donnees = $reponse->fetch();
    $donneesAuteur = $auteur->fetch();
    
    
        echo '<div class= "billet" id='.$donnees['id'].'>
        <h2>'.$donnees['title'].'</h2>
        <img class = "preview" style = "width:200px" src="'.$donnees['image_url'].'" alt="icone">
        <p>'.$donnees['content'].'</p><p>Date :'.$donnees['published_at'].'</p>
        <p>Auteur: '.$donneesAuteur['firstname'] .' ' .$donneesAuteur['lastname'] .'</p>
        <p>Temps de lecture :'.$donnees['reading_time'].'</p>
        <a href="index.php">Accueil</a>';
        while ($cat = $category->fetch()){
        echo '<p>Categorie: '.$cat['category'].'</p>';

        }

           

        echo ' </div>';
   
     ?>
    </main>
    <footer>
        <?php
            //pour lier le footer :
            include __DIR__.'/../includes/footer.php';
        ?>
    </footer>
</body>
</html>
